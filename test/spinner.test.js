import React from "react";
import Spinner from "components/Spinner";
import { render } from "@testing-library/react";

describe('Spinner tests', () => {
  it('Spinner match snap', () => {
    const {asFragment} = render(
      <Spinner/>
    );

    expect(asFragment()).toMatchSnapshot();
  });
});
