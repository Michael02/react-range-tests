# react-range-tests

### Download
##### With GIT
```
git clone https://gitlab.com/Michael02/react-range-tests.git
```
or (if you have [ssh key](https://help.github.com/enterprise/2.12/user/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/))
```
git clone git@gitlab.com:Michael02/react-range-tests.git
```
##### Or just click download button
on top of repository or [here](https://gitlab.com/Michael02/react-range-tests/-/archive/master/react-range-tests-master.zip) to download .zip

### Instalation
```
yarn
```
### Startup
```
yarn dev
```
