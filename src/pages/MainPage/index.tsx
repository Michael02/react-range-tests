import React, { useState } from "react";
import { Container, Row, Col } from "react-bootstrap";

import { ReactRange } from "components/ReactRange/ReactRange";
import { MineRange } from "components/MineRange/MineRange";
import { Slider } from "components/Slider";

type Props = {};

const min = 0;
const max = 49;

const MainPage: React.FC<Props> = () => {
  const [valuesR, setValuesR] = useState([0, 7, 42, 49]);
  return (
    <Container fluid>
      <Row className='pt-3'>
        <Col md='6'>
          <div style={{ margin: "15px 0", height: 15 }}>
            <ReactRange
              values={valuesR}
              onChange={(newVal) => setValuesR(newVal)}
              step={1}
              min={min}
              max={max}
            />
          </div>
        </Col>
      </Row>
      <Row className='mt-3'>
        <Col md='6'>
          <div style={{ margin: "15px 0", height: 15 }}>
            <MineRange
              values={valuesR}
              onChange={(newVal) => setValuesR(newVal)}
              min={min}
              max={max}
              moveStep={2}
              hasDraggableBars
            />
            <p>{valuesR.join(" - ")}</p>
          </div>
        </Col>
      </Row>
      <Row className='mt-3'>
        <Col md='6'>
          <div style={{ margin: "15px 0", height: 15 }}>
            <Slider
              values={valuesR}
              onChange={(values) => {
                setValuesR(values);
              }}
              min={min}
              max={max}
              moveStep={2}
              hasDraggableBars
              hasEqualBars
            />
            <p>{valuesR.join(" - ")}</p>
          </div>
        </Col>
      </Row>
    </Container>
  );
};

export default MainPage;
