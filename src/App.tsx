import { hot } from "react-hot-loader/root";
import { AppContainer } from "react-hot-loader";
import React from "react";

import MainNavigatin from "routing";
import "styles/main.scss";

const App: React.FunctionComponent = () => {
  return (
    <AppContainer>
      <MainNavigatin />
    </AppContainer>
  );
};

export default hot(App);
