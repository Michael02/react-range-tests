import React from "react";
import { Range } from "react-range";
import { ITrackProps, IThumbProps } from "react-range/lib/types";

const MIN_VALUE = 0;
const MAX_VALUE = 50;
const STEP = 1;

type Props = {
  values: number[];
  onChange: (newValues: number[]) => void;
  min?: number;
  max?: number;
  step?: number;
};

export const ReactRange: React.FC<Props> = ({
  values,
  onChange,
  min = MIN_VALUE,
  max = MAX_VALUE,
  step = STEP,
}) => {
  const renderTrack = ({
    props,
    children,
  }: {
    props: ITrackProps;
    children: React.ReactNode;
    isDragged: boolean;
    disabled: boolean;
  }) => {
    return (
      <div
        style={{
          ...props.style,
          height: "15px",
          display: "flex",
          width: "100%",
        }}
        onMouseDown={props.onMouseDown}
        onTouchStart={props.onTouchStart}
      >
        <div
          ref={props.ref}
          style={{
            height: "5px",
            width: "100%",
            borderRadius: "4px",
            background: "#ddd",
            alignSelf: "center",
          }}
        >
          {children}
        </div>
      </div>
    );
  };

  const renderThumb = ({
    props,
  }: {
    props: IThumbProps;
    value: number;
    index: number;
    isDragged: boolean;
  }) => {
    // console.log("thumb props.style: ", props);
    return (
      <div
        {...props}
        style={{
          ...props.style,
          height: "15px",
          width: "15px",
          borderRadius: "50%",
          backgroundColor: "#fff",
          border: "2px solid #5a74ff",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          boxShadow: "0px 2px 6px #AAA",
        }}
      />
    );
  };

  return (
    <Range
      values={values}
      onChange={onChange}
      min={min}
      max={max}
      step={step}
      allowOverlap={false}
      draggableTrack
      renderTrack={renderTrack}
      renderThumb={renderThumb}
    />
  );
};
