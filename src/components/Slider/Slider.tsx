import React, { useEffect, useRef, useState, useMemo, useCallback, RefObject } from "react";

// import { useWindowResize } from "common/hooks";
import { checkInitSlider } from "./SliderError";
import {
  checkValues,
  createNewValuesArray,
  normalizeNewValue,
  schedule,
  setBarWidth,
  setOffset,
} from "./utils";
import {
  MAX_BAR_SELECT,
  MAX_SELECT,
  MAX_VALUE,
  MIN_VALUE,
  MOVE_STEP,
  clearRect,
  THUMB_RADIUS,
  MARK_BORDER_WIDTH,
} from "./constants";
import { SliderMark } from "./components/SliderMark";
import { SliderBar } from "./components/SliderBar";
import { SliderThumb } from "./components/SliderThumb";

import "./Slider.scss";

export type formatterType = (value: number) => string;
const defaultFormatter = (value: number) => `${value}`;

export type Props = {
  values: number[];
  onChange: (newValues: number[]) => void;
  min?: number;
  max?: number;
  moveStep?: number;
  hasBars?: boolean;
  hasDraggableBars?: boolean;
  hasEqualBars?: boolean;
  maxSelect?: number;
  maxBarSelect?: number;
  marks?: number[];
  markFormatter?: formatterType;
  markHideProximity?: number;
  showThumbLabel?: boolean;
  thumbLabelFormatter?: formatterType;
};

export const Slider = ({
  values,
  onChange,
  min = MIN_VALUE,
  max = MAX_VALUE,
  moveStep = MOVE_STEP,
  hasBars = false,
  hasDraggableBars = false,
  hasEqualBars = false,
  maxSelect = MAX_SELECT,
  maxBarSelect = MAX_BAR_SELECT,
  marks = [],
  markFormatter = defaultFormatter,
  markHideProximity = 5,
  showThumbLabel = false,
  thumbLabelFormatter = defaultFormatter,
}: Props) => {
  const sliderTrackRef = useRef<HTMLDivElement>(null);
  const [thumbsRefs, setThumbsRefs] = useState<React.RefObject<HTMLDivElement>[]>([]);
  const [barsRefs, setBarsRefs] = useState<React.RefObject<HTMLDivElement>[]>([]);
  const [draggedThumbIndex, setDraggedThumbIndex] = useState(-1);
  const [draggedBarIndex, setDraggedBarIndex] = useState(-1);
  // const { width: windowWidth } = useWindowResize();

  useEffect(() => {
    checkInitSlider(
      values,
      min,
      max,
      moveStep,
      hasEqualBars,
      hasBars,
      hasDraggableBars,
      maxSelect,
      maxBarSelect,
      marks
    );

    const newThumbsRefs = values.map(() => React.createRef<HTMLDivElement>());
    setThumbsRefs(newThumbsRefs);
    const newBarsRefs =
      hasBars || hasDraggableBars
        ? values
            .slice(0, Math.floor(values.length / 2))
            .map(() => React.createRef<HTMLDivElement>())
        : [];
    setBarsRefs(newBarsRefs);
  }, [values.length, hasBars, hasDraggableBars]);

  const sliderRect = useMemo<DOMRect>(
    () => sliderTrackRef.current?.getBoundingClientRect() || clearRect,
    [sliderTrackRef?.current] // windowWidth
  );

  const translateValue2Offset = useCallback(
    (value: number) => (sliderRect.width / (max - min)) * Math.abs(value - min),
    [sliderRect, min, max]
  );

  const translateOffset2rawValue = useCallback(
    (offset: number) => (offset / sliderRect.width) * (max - min) + min,
    [min, max, sliderRect]
  );

  const normalizeValue = (value: number, index: number, step: number) =>
    normalizeNewValue(value, index, values, step, min, max);

  const keepBarStyleUpdated = useCallback(
    (thumbPosition: number, thumbIndex: number) => {
      const isEndThumb = Boolean(thumbIndex % 2);
      const barIndex = Math.floor(thumbIndex / 2);
      const barRef = barsRefs[barIndex];
      const otherThumbRef = thumbsRefs[isEndThumb ? thumbIndex - 1 : thumbIndex + 1];
      if (barRef?.current && otherThumbRef.current) {
        const otherThumbOffset = Number(otherThumbRef.current.dataset.offset);
        const newWidth = Math.abs(thumbPosition - otherThumbOffset);
        setBarWidth(newWidth, barRef);
        if (!isEndThumb) {
          setOffset(thumbPosition + THUMB_RADIUS, barRef);
        }
      }
    },
    [thumbsRefs, barsRefs]
  );

  const onThumbMove = useCallback(
    (event: MouseEvent, thumbRef: RefObject<HTMLDivElement>, thumbIndex: number) => {
      if (!thumbRef.current) return;
      const offsetLeft = event.clientX - sliderRect.left;
      const newPosition =
        offsetLeft < 0 ? 0 : offsetLeft > sliderRect.width ? sliderRect.width : offsetLeft;
      const finalPosition = newPosition - THUMB_RADIUS;

      const currentRawValue = translateOffset2rawValue(offsetLeft);
      const normalizedValue = normalizeValue(currentRawValue, thumbIndex, 1);
      const finalValue = normalizeValue(Math.round(normalizedValue), thumbIndex, 1);

      const newValues = createNewValuesArray(
        finalValue,
        thumbIndex,
        values,
        hasEqualBars,
        barsRefs,
        min,
        max
      );
      const areCorrectValues = checkValues(newValues, {
        hasEqualBars,
        maxSelect,
        maxBarSelect,
        hasBars: hasBars || hasDraggableBars,
      });

      if (currentRawValue === normalizedValue && areCorrectValues) {
        setOffset(finalPosition, thumbRef);
        if (hasBars || hasDraggableBars) {
          keepBarStyleUpdated(finalPosition, thumbIndex);
        }
      }
      if (areCorrectValues && !(Math.abs(values[thumbIndex] - finalValue) % moveStep)) {
        thumbRef.current.setAttribute("data-value", `${finalValue}`);
        onChange(newValues);
      }
    },
    [
      values,
      moveStep,
      sliderRect,
      onChange,
      normalizeValue,
      translateOffset2rawValue,
      hasBars,
      hasDraggableBars,
      hasEqualBars,
      keepBarStyleUpdated,
      barsRefs,
    ]
  );

  const onMouseDownThumb = useCallback(
    (thumbRef: RefObject<HTMLDivElement>, thumbIndex: number) => {
      setDraggedThumbIndex(thumbIndex);
      function onMove(e: MouseEvent) {
        onThumbMove(e, thumbRef, thumbIndex);
      }
      const schdOnMove = schedule(onMove);
      document.addEventListener("mousemove", schdOnMove);

      document.addEventListener(
        "mouseup",
        () => {
          setDraggedThumbIndex(-1);
          document.removeEventListener("mousemove", schdOnMove);
          schdOnMove.cancel();
          if (!thumbRef.current) return;
          const finalValue = Number(thumbRef.current.dataset.value);
          const finalOffset = translateValue2Offset(finalValue) - THUMB_RADIUS;
          setOffset(finalOffset, thumbRef);
          if (hasBars || hasDraggableBars) {
            keepBarStyleUpdated(finalOffset, thumbIndex);
          }
        },
        { once: true }
      );
    },
    [onThumbMove, keepBarStyleUpdated, translateValue2Offset, hasBars, hasDraggableBars]
  );

  const onBarMove = useCallback(
    (
      event: MouseEvent,
      initialClickXPosition: number,
      barRef: RefObject<HTMLDivElement>,
      thumbStartRef: RefObject<HTMLDivElement>,
      thumbEndRef: RefObject<HTMLDivElement>,
      barSpecs: {
        values: { start: number; end: number; diff: number };
        width: number;
        thumbsOffsets: { start: number; end: number };
      },
      thumbsIndexes: [number, number]
    ) => {
      if (!(barRef.current && thumbStartRef.current && thumbEndRef.current)) return;
      const isIncreasing = 0 < event.clientX - initialClickXPosition;
      const [thumbStartIndex, thumbEndIndex] = thumbsIndexes;
      const outIndex = -2;

      const offsetLeft = event.clientX - sliderRect.left;
      let newStartPosition = offsetLeft + barSpecs.thumbsOffsets.start;
      let newEndPosition = newStartPosition + barSpecs.width;
      if (newStartPosition < 0) {
        newStartPosition = 0;
        newEndPosition = barSpecs.width;
      } else if (newEndPosition > sliderRect.width) {
        newEndPosition = sliderRect.width;
        newStartPosition = newEndPosition - barSpecs.width;
      }
      const startRawValue = translateOffset2rawValue(newStartPosition);
      const startNormalizedValue = normalizeValue(
        startRawValue,
        isIncreasing ? outIndex : thumbStartIndex,
        1
      );
      const endRawValue = translateOffset2rawValue(newEndPosition);
      const endNormalizedValue = normalizeValue(
        endRawValue,
        isIncreasing ? thumbEndIndex : outIndex,
        1
      );

      if (startRawValue === startNormalizedValue && endRawValue === endNormalizedValue) {
        setOffset(newStartPosition - THUMB_RADIUS, thumbStartRef);
        setOffset(newEndPosition - THUMB_RADIUS, thumbEndRef);
        setOffset(newStartPosition, barRef);
      }
      const startFinalValue = normalizeValue(
        Math.round(startNormalizedValue),
        isIncreasing ? outIndex : thumbStartIndex,
        1
      );
      const endFinalValue = normalizeValue(
        Math.round(endNormalizedValue),
        isIncreasing ? thumbEndIndex : outIndex,
        1
      );
      if (
        endFinalValue - startFinalValue === barSpecs.values.diff &&
        !(
          Math.abs(barSpecs.values.start - startFinalValue) % moveStep ||
          Math.abs(barSpecs.values.end - endFinalValue) % moveStep
        )
      ) {
        thumbStartRef.current.dataset.value = `${startFinalValue}`;
        thumbEndRef.current.dataset.value = `${endFinalValue}`;
        const newArray = [...values];
        newArray[thumbStartIndex] = startFinalValue;
        newArray[thumbEndIndex] = endFinalValue;
        onChange(newArray);
      }
    },
    [sliderRect, values, moveStep, onChange, normalizeValue, translateOffset2rawValue]
  );

  const onMouseDownBar = useCallback(
    (
      event: React.MouseEvent,
      barRef: RefObject<HTMLDivElement>,
      barIndex: number,
      thumbsIndexes: [number, number]
    ) => {
      setDraggedBarIndex(barIndex);
      const relativeClickPosition = event.clientX - sliderRect.left;
      const [thumbStartIndex, thumbEndIndex] = thumbsIndexes;
      const thumbStart = thumbsRefs[thumbStartIndex];
      const thumbEnd = thumbsRefs[thumbEndIndex];
      if (!(barRef.current && thumbStart.current && thumbEnd.current)) return;

      const thumbStartOffset = Number(thumbStart.current.dataset.offset);
      const thumbEndOffset = Number(thumbEnd.current.dataset.offset);
      const thumbStartValue = Number(thumbStart.current.dataset.value);
      const thumbEndValue = Number(thumbEnd.current.dataset.value);
      const barWidth = Number(barRef.current.dataset.width);
      const barSpecs = {
        values: {
          start: thumbStartValue,
          end: thumbEndValue,
          diff: thumbEndValue - thumbStartValue,
        },
        width: barWidth,
        thumbsOffsets: {
          start: thumbStartOffset - relativeClickPosition,
          end: thumbEndOffset - relativeClickPosition,
        },
      };

      function onMove(e: MouseEvent) {
        onBarMove(e, event.clientX, barRef, thumbStart, thumbEnd, barSpecs, thumbsIndexes);
      }
      const schdOnMove = schedule(onMove);
      document.addEventListener("mousemove", schdOnMove);

      document.addEventListener(
        "mouseup",
        () => {
          setDraggedBarIndex(-1);
          document.removeEventListener("mousemove", schdOnMove);
          schdOnMove.cancel();
          if (!(barRef.current && thumbStart.current && thumbEnd.current)) return;
          const startValue = Number(thumbStart.current.dataset.value);
          const endValue = Number(thumbEnd.current.dataset.value);
          const startOffset = translateValue2Offset(startValue);
          const endOffset = translateValue2Offset(endValue);
          setOffset(startOffset - THUMB_RADIUS, thumbStart);
          setOffset(endOffset - THUMB_RADIUS, thumbEnd);
          setOffset(startOffset, barRef);
        },
        { once: true }
      );
    },
    [sliderRect, thumbsRefs, translateValue2Offset, onBarMove]
  );

  const marksList = marks.map((mark, idx) => {
    return (
      <SliderMark
        key={`mark_${idx}`}
        markLabel={markFormatter(mark)}
        markOffset={translateValue2Offset(mark) - MARK_BORDER_WIDTH}
        isHiddenLabel={showThumbLabel && values.some((v) => Math.abs(mark - v) < markHideProximity)}
      />
    );
  });

  const barsList = barsRefs.map((ref, idx) => (
    <SliderBar
      key={`bar_${idx}`}
      index={idx}
      barRef={ref}
      hasDraggableBars={hasDraggableBars}
      allValues={values}
      translateValue2Offset={translateValue2Offset}
      onMouseDownBar={onMouseDownBar}
      draggedBarIndex={draggedBarIndex}
      draggedThumbIndex={draggedThumbIndex}
    />
  ));

  const thumbsList = thumbsRefs.map((ref, idx) => (
    <SliderThumb
      key={`thumb_${idx}`}
      value={values[idx]}
      index={idx}
      thumbRef={ref}
      onMouseDownThumb={onMouseDownThumb}
      draggedThumbIndex={draggedThumbIndex}
      draggedBarIndex={draggedBarIndex}
      translateValue2Offset={translateValue2Offset}
      showThumbLabel={showThumbLabel}
      thumbLabelFormatter={thumbLabelFormatter}
    />
  ));

  return (
    <div className='slider' ref={sliderTrackRef}>
      <div className='slider__track'>
        {marksList}
        {barsList}
        {thumbsList}
      </div>
    </div>
  );
};
