import React, { useState, useEffect, useCallback, RefObject } from "react";
import classNames from "classnames";

import { formatterType } from "../Slider";
import { THUMB_RADIUS } from "../constants";

type Props = {
  value: number;
  index: number;
  onMouseDownThumb: (thumbRef: React.RefObject<HTMLDivElement>, thumbIndex: number) => void;
  draggedThumbIndex: number;
  draggedBarIndex: number;
  thumbRef: RefObject<HTMLDivElement>;
  translateValue2Offset: (value: number) => number;
  showThumbLabel: boolean;
  thumbLabelFormatter: formatterType;
};

export const SliderThumb = ({
  value,
  index,
  onMouseDownThumb,
  draggedThumbIndex,
  draggedBarIndex,
  translateValue2Offset,
  showThumbLabel,
  thumbLabelFormatter,
  thumbRef,
}: Props) => {
  const [position, setPosition] = useState(0);

  useEffect(() => {
    const barIndex = Math.floor(index / 2);
    if (draggedThumbIndex !== index && barIndex !== draggedBarIndex) {
      setPosition(translateValue2Offset(value) - THUMB_RADIUS);
    } else {
      setPosition(Number(thumbRef.current?.dataset.offset));
    }
  }, [value, translateValue2Offset]);

  const onMouseDown = useCallback(() => {
    onMouseDownThumb(thumbRef, index);
  }, [index, onMouseDownThumb]);

  return (
    <div
      ref={thumbRef}
      className={classNames("slider__thumb", { dragged: draggedThumbIndex === index })}
      style={{
        transform: `translateX(${position}px)`,
      }}
      data-value={`${value}`}
      data-offset={`${position}`}
      onMouseDown={onMouseDown}
    >
      {showThumbLabel && <span className='slider__thumb__label'>{thumbLabelFormatter(value)}</span>}
    </div>
  );
};
