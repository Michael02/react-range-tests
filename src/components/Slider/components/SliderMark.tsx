import React, { memo } from "react";
import classNames from "classnames";

type Props = {
  markOffset: number;
  markLabel: string;
  isHiddenLabel: boolean;
};

export const SliderMark = memo(({ markOffset, markLabel, isHiddenLabel }: Props) => {
  return (
    <div className='slider__mark' style={{ transform: `translate(${markOffset}px, 0)` }}>
      <span className={classNames("slider__mark__label", { hidden: isHiddenLabel })}>
        {markLabel}
      </span>
    </div>
  );
});
