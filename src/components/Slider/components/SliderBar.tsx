import React, { RefObject, useCallback, useEffect, useMemo, useState } from "react";
import classNames from "classnames";

type Props = {
  index: number;
  hasDraggableBars: boolean;
  allValues: number[];
  translateValue2Offset: (value: number) => number;
  onMouseDownBar: (
    event: React.MouseEvent<Element, MouseEvent>,
    barRef: React.RefObject<HTMLDivElement>,
    barIndex: number,
    thumbsIndexes: [number, number]
  ) => void;
  barRef: RefObject<HTMLDivElement>;
  draggedBarIndex: number;
  draggedThumbIndex: number;
};

export const SliderBar = ({
  index,
  hasDraggableBars,
  allValues,
  translateValue2Offset,
  onMouseDownBar,
  draggedBarIndex,
  draggedThumbIndex,
  barRef,
}: Props) => {
  const [position, setPosition] = useState(0);
  const [width, setWidth] = useState(0);

  const thumbsIndexes: [number, number] = useMemo(() => {
    const startIndex = index * 2;
    const endIndex = startIndex + 1;
    return [startIndex, endIndex];
  }, [index]);

  useEffect(() => {
    if (draggedBarIndex !== index && !thumbsIndexes.includes(draggedThumbIndex)) {
      const posStart = translateValue2Offset(allValues[thumbsIndexes[0]]);
      const posEnd = translateValue2Offset(allValues[thumbsIndexes[1]]);
      setPosition(posStart);
      setWidth(posEnd - posStart);
    } else {
      setPosition(Number(barRef.current?.dataset.offset));
      setWidth(Number(barRef.current?.dataset.width));
    }
  }, [allValues, index, translateValue2Offset, thumbsIndexes, draggedThumbIndex]);

  const onMouseDown = useCallback(
    (e: React.MouseEvent) => {
      if (!hasDraggableBars) return;
      onMouseDownBar(e, barRef, index, thumbsIndexes);
    },
    [index, onMouseDownBar]
  );

  const barStyle: React.CSSProperties = {
    width: `${width}px`,
    transform: `translateX(${position}px)`,
  };

  return (
    <div
      className={classNames("slider__bar", { draggable: hasDraggableBars })}
      ref={barRef}
      style={barStyle}
      data-offset={position}
      data-width={width}
      onMouseDown={onMouseDown}
    />
  );
};
