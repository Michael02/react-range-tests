import { noop } from "lodash";

export const MIN_VALUE = 0;
export const MAX_VALUE = 50;
export const MOVE_STEP = 1;
export const MAX_SELECT = 0;
export const MAX_BAR_SELECT = 0;

export const THUMB_RADIUS = 5.5;
export const MARK_BORDER_WIDTH = 1;

export const clearRect: DOMRect = {
  bottom: 0,
  height: 0,
  left: 0,
  right: 0,
  top: 0,
  width: 0,
  x: 0,
  y: 0,
  toJSON: noop,
};
