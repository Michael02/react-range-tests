import React, { useState } from "react";
import { render } from "@testing-library/react";

import { Slider, Props as SliderProps } from "./Slider";
import { THUMB_RADIUS, clearRect } from "./constants";
import { chunkArray } from "./utils";

export const TEST_TRACK_WIDTH = 500;
export const testRect: DOMRect = {
  ...clearRect,
  width: TEST_TRACK_WIDTH,
};

const TestSlider = ({
  defaultValues,
  min = 0,
  max = 50,
  moveStep = 1,
  maxSelect,
  maxBarSelect,
  marks,
  showThumbLabel = false,
  hasBars = false,
  hasDraggableBars = false,
  hasEqualBars = false,
}: { defaultValues: number[] } & Partial<SliderProps>) => {
  const [values, setValues] = useState(defaultValues);
  return (
    <div data-testid='slider-container'>
      <Slider
        values={values}
        onChange={(newValues) => setValues(newValues)}
        min={min}
        max={max}
        moveStep={moveStep}
        maxSelect={maxSelect}
        maxBarSelect={maxBarSelect}
        marks={marks}
        showThumbLabel={showThumbLabel}
        hasBars={hasBars}
        hasDraggableBars={hasDraggableBars}
        hasEqualBars={hasEqualBars}
      />
    </div>
  );
};

describe("Slider Tests", () => {
  beforeEach(() => {
    Element.prototype.getBoundingClientRect = jest.fn(() => testRect);
  });

  it("Slider reders correct number of thumbs and bars", () => {
    const { getByTestId } = render(
      <TestSlider defaultValues={[5, 15, 30, 50]} hasBars marks={[0, 25, 50]} />
    );
    const sliderContainer = getByTestId("slider-container");
    const thumbs = sliderContainer.querySelectorAll(".slider__thumb");
    const bars = sliderContainer.querySelectorAll(".slider__bar");
    const marks = sliderContainer.querySelectorAll(".slider__mark");
    expect(thumbs.length).toEqual(4);
    expect(marks.length).toEqual(3);
    expect(bars.length).toEqual(2);
  });

  it("Slider's thumbs have correct offset", () => {
    const values = [5, 15, 30, 50];
    const { getByTestId } = render(<TestSlider defaultValues={values} />);
    const sliderContainer = getByTestId("slider-container");
    const positions = values.map((val) => (TEST_TRACK_WIDTH / 50) * val - THUMB_RADIUS);
    const thumbs = sliderContainer.querySelectorAll(".slider__thumb");

    thumbs.forEach((thumb, idx) => {
      const offset = Number(thumb.getAttribute("data-offset"));
      expect(offset).toEqual(positions[idx]);
    });
  });

  it("Slider's bars have correct possition & length", () => {
    const values = [-15, 25, 75, 90];
    const { getByTestId } = render(
      <TestSlider defaultValues={values} hasBars min={-100} max={100} />
    );
    const sliderContainer = getByTestId("slider-container");
    const positions = values.map((val) => (TEST_TRACK_WIDTH / 200) * (val + 100));
    const bars = sliderContainer.querySelectorAll(".slider__bar");
    const barsPointsPositions = chunkArray(positions, 2);

    bars.forEach((bar, idx) => {
      const offset = Number(bar.getAttribute("data-offset"));
      const width = Number(bar.getAttribute("data-width"));
      const [startPoint, endPoint] = barsPointsPositions[idx];
      expect(offset).toEqual(startPoint);
      expect(width).toEqual(endPoint - startPoint);
    });
  });
});
