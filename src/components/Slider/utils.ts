import { RefObject } from "react";
import { SliderError } from "./SliderError";

export const chunkArray = <A>(array: A[], chunk: number) => {
  const chuncked: A[][] = [];
  for (let i = 0; i <= array.length - chunk; i += chunk) {
    chuncked.push(array.slice(i, i + chunk));
  }
  return chuncked;
};

// function copied from react-range
// and they say it's based on https://github.com/alexreardon/raf-schd
// don't ask me what it does, I guess it should make functions in listeners work more smoothly
export const schedule = (fn: Function) => {
  let lastArgs: any[] = [];
  let frameId: number | null = null;
  const wrapperFn = (...args: any[]) => {
    lastArgs = args;
    if (frameId) {
      return;
    }
    frameId = requestAnimationFrame(() => {
      frameId = null;
      fn(...lastArgs);
    });
  };
  wrapperFn.cancel = () => {
    if (!frameId) return;
    cancelAnimationFrame(frameId);
    frameId = null;
  };
  return wrapperFn;
};

export const normalizeNewValue = (
  value: number,
  thumbIndex: number,
  values: number[],
  moveStep: number,
  min: number,
  max: number
) => {
  const prev = values[thumbIndex - 1];
  const next = values[thumbIndex + 1];
  if (prev && value < prev + moveStep) return prev + moveStep;
  if (next && value > next - moveStep) return next - moveStep;
  if (value >= max) return max;
  if (value <= min) return min;
  return value;
};

export const setOffset = (offset: number, elemRef: RefObject<HTMLDivElement>) => {
  if (!elemRef.current) return;

  elemRef.current.style.transform = `translateX(${offset}px)`;
  elemRef.current.dataset.offset = `${offset}`;
};

export const setBarWidth = (width: number, barRef: RefObject<HTMLDivElement>) => {
  if (!barRef.current) return;

  barRef.current.style.width = `${width}px`;
  barRef.current.dataset.width = `${width}`;
};

export const createNewValuesArray = (
  newValue: number,
  thumbIndex: number,
  values: number[],
  hasEqualBars: boolean,
  barsRefs: RefObject<HTMLDivElement>[],
  min: number,
  max: number
) => {
  const newArray = [...values];
  newArray[thumbIndex] = newValue;
  const valueDiff = newValue - values[thumbIndex];

  if (hasEqualBars && valueDiff) {
    const isEndThumb = Boolean(thumbIndex % 2);
    const currentBarIndex = Math.floor(thumbIndex / 2);
    barsRefs.map((barRef, idx) => {
      if (idx !== currentBarIndex) {
        const barStartThumbIndex = idx * 2;
        const barEndThumbIndex = barStartThumbIndex + 1;
        const prevValue = values[barStartThumbIndex - 1];
        const nextValue = values[barEndThumbIndex + 1];
        if (isEndThumb) {
          const newValue = values[barEndThumbIndex] + valueDiff;
          if ((nextValue && newValue < nextValue) || (!nextValue && newValue <= max)) {
            newArray[barEndThumbIndex] = newValue;
          } else {
            const availableSpaceAfter = nextValue
              ? nextValue - (values[barEndThumbIndex] + 1)
              : max - values[barEndThumbIndex];
            const requiredSpaceBefore = valueDiff - availableSpaceAfter;
            const newStartValue = values[barStartThumbIndex] - requiredSpaceBefore;
            const newEndValue = nextValue ? nextValue - 1 : max;
            if ((prevValue && newStartValue > prevValue) || (!prevValue && newStartValue >= min)) {
              newArray[barStartThumbIndex] = newStartValue;
              newArray[barEndThumbIndex] = newEndValue;
            } else {
              newArray[barEndThumbIndex] = min;
              newArray[barStartThumbIndex] = max;
            }
          }
        } else {
          const newValue = values[barStartThumbIndex] + valueDiff;
          if ((prevValue && newValue > prevValue) || (!prevValue && newValue >= min)) {
            newArray[barStartThumbIndex] = newValue;
          } else {
            const availableSpaceBefore = prevValue
              ? values[barStartThumbIndex] - (prevValue + 1)
              : values[barStartThumbIndex] - min;
            const requiredSpaceAfter = Math.abs(valueDiff) - availableSpaceBefore;
            const newStartValue = prevValue ? prevValue + 1 : min;
            const newEndValue = values[barEndThumbIndex] + requiredSpaceAfter;
            if ((nextValue && newEndValue < nextValue) || (!nextValue && newEndValue <= max)) {
              newArray[barStartThumbIndex] = newStartValue;
              newArray[barEndThumbIndex] = newEndValue;
            } else {
              newArray[barEndThumbIndex] = min;
              newArray[barStartThumbIndex] = max;
            }
          }
        }
      }
    });
  }

  return newArray;
};

export const checkValues = (
  values: number[],
  options: {
    hasBars?: boolean;
    hasEqualBars?: boolean;
    maxSelect?: number;
    maxBarSelect?: number;
    throwError?: boolean;
  } = {}
): boolean => {
  const isOrderCorrect = values.slice(1).every((val, idx) => val > values[idx]);
  const {
    hasBars = false,
    hasEqualBars = false,
    maxBarSelect = 0,
    maxSelect = 0,
    throwError = false,
  } = options;

  if (!hasBars || (!maxSelect && !maxBarSelect && !hasEqualBars)) {
    if (!isOrderCorrect && throwError) {
      throw new SliderError(`Values [${values}] have to be sorted`);
    }
    return isOrderCorrect;
  }

  const barsLength = chunkArray(values, 2).reduce<number[]>((output, item) => {
    output.push(item[1] - item[0]);
    return output;
  }, []);

  const areEqualBars = barsLength.slice(1).every((val, idx) => val === barsLength[idx]);
  const isOrederAndEqual = isOrderCorrect && (!hasEqualBars || areEqualBars);
  if (!isOrederAndEqual && throwError) {
    throw new SliderError(
      `Length of each bar must be EQUAL, while attribute hasEqualBars is true. Bars lengths: [${barsLength}] for values={[${values}]}`
    );
  }

  if (maxBarSelect) {
    const barsLengthsCheck = barsLength.every((item) => item <= maxBarSelect);
    if (!barsLengthsCheck && throwError) {
      throw new SliderError(
        `Length of each bar cannot be larger than ${maxBarSelect} (maxBarSelect={${maxBarSelect}}). Bars lengths: [${barsLength}] for values={[${values}]}`
      );
    }
    return isOrederAndEqual && barsLengthsCheck;
  }

  if (maxSelect) {
    const selectedLengthCheck =
      barsLength.reduce<number>((output, item) => output + item, 0) <= maxSelect;
    if (!selectedLengthCheck && throwError) {
      throw new SliderError(
        `Cumulative length of bars cannot be greater than ${maxSelect} (maxSelect={${maxSelect}}). Bars lengths: [${barsLength}] for values={[${values}]}`
      );
    }
    return isOrederAndEqual && selectedLengthCheck;
  }

  return isOrederAndEqual;
};
