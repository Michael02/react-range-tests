import { checkValues } from "./utils";

export class SliderError extends Error {
  constructor(message: string) {
    super(message);
    this.name = "SliderError";
  }
}

export const checkInitSlider = (
  values: number[],
  min: number,
  max: number,
  moveStep: number,
  hasEqualBars: boolean,
  hasBars: boolean,
  hasDraggableBars: boolean,
  maxSelect: number,
  maxBarSelect: number,
  marks: number[]
) => {
  if (min >= max) {
    throw new SliderError(
      `Minimal value (attribute min={${min}}) must be smaller than max (max={${max}})`
    );
  }
  checkValues(values, {
    hasEqualBars,
    maxSelect,
    maxBarSelect,
    hasBars: hasBars || hasDraggableBars,
    throwError: true,
  });
  if (values.some((val) => val < min || val > max)) {
    throw new SliderError(`Wrong values. All values [${values}] must be between ${min} and ${max}`);
  }
  if (marks.some((val) => val < min || val > max)) {
    throw new SliderError(`Wrong marks. All marks [${marks}] must be between ${min} and ${max}`);
  }
  if ((hasBars || hasDraggableBars) && values.length % 2) {
    throw new SliderError(
      "Wrong number of values. While enabling bars, the number of values must be even."
    );
  }
  if (!Number.isInteger(moveStep)) {
    throw new SliderError(`Attribute "moveStep" (moveStep={${moveStep}}) must be an Integer`);
  }
};
