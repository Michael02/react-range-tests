import React, { useEffect, useRef, useState } from "react";

import { SliderTrack } from "./components/SliderTrack";
import { MAX_VALUE, MIN_VALUE, MOVE_STEP, SliderContextProvider } from "./context/SliderContext";

import "./MineRange.scss";

type Props = {
  values: number[];
  onChange: (newValues: number[]) => void;
  min?: number;
  max?: number;
  moveStep?: number;
  hasBars?: boolean;
  hasDraggableBars?: boolean;
  hasEqualBars?: boolean;
};

export const MineRange: React.FC<Props> = ({
  values,
  onChange,
  min = MIN_VALUE,
  max = MAX_VALUE,
  moveStep = MOVE_STEP,
  hasBars = false,
  hasDraggableBars = false,
  hasEqualBars = false,
}) => {
  const sliderTrackRef = useRef<HTMLDivElement>(null);
  const [ref_state, setRefState] = useState(undefined);
  const [thumbsRefs, setThumbsRefs] = useState<React.RefObject<HTMLDivElement>[]>([]);
  const [barsRefs, setBarsRefs] = useState<React.RefObject<HTMLDivElement>[]>([]);

  useEffect(() => {
    const newThumbsRefs = values.map(() => React.createRef<HTMLDivElement>());
    setThumbsRefs(newThumbsRefs);
    const newBarsRefs = values
      .slice(0, Math.floor(values.length / 2))
      .map(() => React.createRef<HTMLDivElement>());
    setBarsRefs(newBarsRefs);
  }, [values.length]);

  useEffect(() => {
    setRefState(sliderTrackRef);
  }, [sliderTrackRef.current]);

  return (
    <div className='slider' ref={sliderTrackRef}>
      <SliderContextProvider
        values={values}
        onChange={onChange}
        min={min}
        max={max}
        moveStep={moveStep}
        hasBars={hasBars}
        hasDraggableBars={hasDraggableBars}
        hasEqualBars={hasEqualBars}
        trackRef={ref_state}
        thumbsRefs={thumbsRefs}
        barsRefs={barsRefs}
      >
        <SliderTrack />
      </SliderContextProvider>
    </div>
  );
};
