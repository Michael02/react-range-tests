import React, { createContext, RefObject, useCallback, useMemo, useState } from "react";
import { noop } from "lodash";
import { getClosestValue, schedule, normalizeNewValue } from "../utils";

export const MIN_VALUE = 0;
export const MAX_VALUE = 50;
export const MOVE_STEP = 1;

type SliderContextProps = {
  values: number[];
  thumbsRefs: RefObject<HTMLDivElement>[];
  draggedThumbIndex: number;
  barsRefs: RefObject<HTMLDivElement>[];
  min: number;
  max: number;
  moveStep: number;
  hasBars: boolean;
  hasDraggableBars: boolean;
  sliderRect: DOMRect;
  onMouseDownThumb: (thumbRef: RefObject<HTMLDivElement>, thumbIndex: number) => void;
  onMouseDownBar: (barRef: RefObject<HTMLDivElement>, barIndex: number) => void;
  translateValue2Offset: (value: number) => number;
};

export const SliderContext = createContext<SliderContextProps>({
  values: [],
  thumbsRefs: [],
  draggedThumbIndex: -1,
  barsRefs: [],
  min: MIN_VALUE,
  max: MAX_VALUE,
  moveStep: MOVE_STEP,
  hasBars: false,
  hasDraggableBars: false,
  sliderRect: new DOMRect(),
  onMouseDownThumb: noop,
  onMouseDownBar: noop,
  translateValue2Offset: (v) => 0,
});

type SliderContextProviderProps = {
  values: number[];
  onChange: (newValues: number[]) => void;
  min: number;
  max: number;
  moveStep: number;
  hasBars: boolean;
  hasDraggableBars: boolean;
  hasEqualBars: boolean;
  trackRef: RefObject<HTMLDivElement> | undefined;
  thumbsRefs: RefObject<HTMLDivElement>[];
  barsRefs: RefObject<HTMLDivElement>[];
};

export const SliderContextProvider: React.FC<SliderContextProviderProps> = ({
  children,
  values,
  onChange,
  min,
  max,
  moveStep,
  hasBars = false,
  hasDraggableBars = false,
  hasEqualBars = false,
  trackRef,
  thumbsRefs,
  barsRefs,
}) => {
  const [draggedThumbIndex, setDraggedThumbIndex] = useState(-1);
  const sliderRect = useMemo<DOMRect>(() => {
    if (!trackRef?.current) {
      return new DOMRect();
    }

    return trackRef.current.getBoundingClientRect();
  }, [trackRef?.current]);

  const translateValue2Offset = useCallback(
    (value: number) => {
      return (sliderRect.width / (max - min)) * Math.abs(value - min);
    },
    [sliderRect, min, max]
  );

  const normalizeValue = useCallback(
    (value: number, index: number, step: number) => {
      return normalizeNewValue(value, index, values, step, min, max);
    },
    [values, min, max]
  );

  const onThumbMove = useCallback(
    (event: MouseEvent, thumbRef: RefObject<HTMLDivElement>, thumbIndex: number) => {
      const offsetLeft = event.clientX - sliderRect.left;
      const newPosition =
        offsetLeft < 0 ? 0 : offsetLeft > sliderRect.width ? sliderRect.width : offsetLeft;

      const currentRawValue = (offsetLeft / sliderRect.width) * (max - min) + min;
      const normalizedValue = normalizeValue(currentRawValue, thumbIndex, 1);
      if (currentRawValue === normalizedValue) {
        thumbRef.current.style.transform = `translate(${newPosition - 7}px, -5px)`;
        thumbRef.current.dataset.offset = `${newPosition - 7}`;
      }
      const finalValue = normalizeValue(getClosestValue(normalizedValue, 1), thumbIndex, 1);
      if (!(Math.abs(values[thumbIndex] - finalValue) % moveStep)) {
        thumbRef.current.setAttribute("data-value", `${finalValue}`);
        const newArray = [...values];
        newArray[thumbIndex] = finalValue;
        onChange(newArray);
      }
    },
    [values, moveStep, sliderRect, min, max, onChange]
  );

  const onMouseDownThumb = useCallback(
    (thumbRef: RefObject<HTMLDivElement>, thumbIndex: number) => {
      setDraggedThumbIndex(thumbIndex);
      function onMove(e: MouseEvent) {
        onThumbMove(e, thumbRef, thumbIndex);
      }
      const schdOnMove = schedule(onMove);
      document.addEventListener("mousemove", schdOnMove);

      document.addEventListener(
        "mouseup",
        () => {
          setDraggedThumbIndex(-1);
          document.removeEventListener("mousemove", schdOnMove);
          const finalValue = Number(thumbRef.current.getAttribute("data-value"));
          thumbRef.current.style.transform = `translate(${
            translateValue2Offset(finalValue) - 7
          }px, -5px)`;
        },
        { once: true }
      );
    },
    [onThumbMove, min, max]
  );
  const onMouseDownBar = useCallback(() => {}, []);

  const value = useMemo(() => {
    return {
      values,
      onChange,
      min,
      max,
      moveStep,
      hasBars,
      hasDraggableBars,
      sliderRect,
      onMouseDownThumb,
      onMouseDownBar,
      thumbsRefs,
      draggedThumbIndex,
      barsRefs,
      translateValue2Offset,
    };
  }, [
    values,
    onChange,
    min,
    max,
    moveStep,
    hasBars,
    hasDraggableBars,
    sliderRect,
    onMouseDownThumb,
    onMouseDownBar,
    thumbsRefs,
    draggedThumbIndex,
    barsRefs,
    translateValue2Offset,
  ]);
  return <SliderContext.Provider value={value}>{children}</SliderContext.Provider>;
};
