import { RefObject } from "react";

export const chunkArray = (array: any[], chunk: number) => {
  const chuncked = array.reduce<any[]>((output, el, idx) => {
    if (!(idx % chunk)) {
      output.push(array.slice(idx, idx + chunk));
    }

    return output;
  }, []);
  return chuncked;
};

// function copied from react-range
// and they say it's based on https://github.com/alexreardon/raf-schd
// don't ask me what it does, I guess it should make functions in listeners work more smoothly
export const schedule = (fn: Function) => {
  let lastArgs: any[] = [];
  let frameId: number | null = null;
  const wrapperFn = function () {
    const args = [];
    // tslint:disable-next-line: no-increment-decrement
    for (let i = 0; i < arguments.length; i++) {
      args[i] = arguments[i];
    }
    lastArgs = args;
    if (frameId) {
      return;
    }
    frameId = requestAnimationFrame(() => {
      frameId = null;
      fn.apply(void 0, lastArgs);
    });
  };
  return wrapperFn;
};

export const getClosestValue = (currentValue: number, step: number) => {
  const q = Math.floor(currentValue / step);

  const close1 = step * q;
  const close2 = currentValue * step > 0 ? step * (q + 1) : step * (q - 1);

  return Math.abs(currentValue - close1) < Math.abs(currentValue - close2) ? close1 : close2;
};

export const normalizeNewValue = (
  value: number,
  thumbIndex: number,
  values: number[],
  moveStep: number,
  min: number,
  max: number
) => {
  const prev = values[thumbIndex - 1];
  const next = values[thumbIndex + 1];
  if (prev && value < prev + moveStep) return prev + moveStep;
  if (next && value > next - moveStep) return next - moveStep;
  if (value >= max) return max;
  if (value <= min) return min;
  return value;
};
