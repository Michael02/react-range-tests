import React, { useState, useContext, forwardRef, useEffect } from "react";
import classNames from "classnames";

import { SliderContext } from "../context/SliderContext";
import { schedule } from "../utils";

type Props = {
  value: number;
  index: number;
};

export const SliderThumb = forwardRef<HTMLDivElement, Props>(({ value, index }, thumbRef) => {
  const { onMouseDownThumb, draggedThumbIndex, thumbsRefs, translateValue2Offset } = useContext(
    SliderContext
  );
  const [position, setPosition] = useState(0);

  useEffect(() => {
    const currentRef = thumbsRefs[index];
    if (draggedThumbIndex !== index) {
      setPosition(translateValue2Offset(value) - 7);
    } else {
      setPosition(Number(currentRef.current.dataset.offset));
    }
  }, [value, thumbsRefs]);

  const onKeyDown = () => {
    const currentRef = thumbsRefs[index];
    onMouseDownThumb(currentRef, index);
  };

  return (
    <div
      ref={thumbRef}
      className={classNames("slider-thumb", { dragged: draggedThumbIndex === index })}
      style={{
        transform: `translate(${position}px, -5px)`,
      }}
      data-value={`${value}`}
      data-offset={`${position}`}
      onMouseDown={schedule(onKeyDown)}
    />
  );
});
