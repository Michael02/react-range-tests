import React, { useContext } from "react";

import { SliderContext } from "../context/SliderContext";
import { SliderBar } from "./SliderBar";
import { SliderThumb } from "./SliderThumb";

export const SliderTrack = () => {
  const { values, thumbsRefs, barsRefs, hasBars, hasDraggableBars } = useContext(SliderContext);
  return (
    <div className='slider-track'>
      {(hasBars || hasDraggableBars) &&
        barsRefs.map((ref, idx) => <SliderBar key={idx} index={idx} ref={ref} />)}
      {thumbsRefs.map((ref, idx) => (
        <SliderThumb key={idx} value={values[idx]} index={idx} ref={ref} />
      ))}
    </div>
  );
};
