import React, { forwardRef, useContext, useEffect, useMemo, useState } from "react";
import classNames from "classnames";

import { SliderContext } from "../context/SliderContext";

type Props = {
  index: number;
};

export const SliderBar = forwardRef<HTMLDivElement, Props>(({ index }, barRef) => {
  const { hasDraggableBars, values, translateValue2Offset } = useContext(SliderContext);
  const [position, setPosition] = useState(0);
  const [width, setWidth] = useState(0);

  const thumbsIndexes = useMemo(() => {
    const startIndex = index * 2;
    const endIndex = startIndex + 1;
    return [startIndex, endIndex];
  }, [index]);

  const barValues = useMemo(() => {
    return {
      start: values[thumbsIndexes[0]],
      end: values[thumbsIndexes[1]],
    };
  }, [values]);

  useEffect(() => {
    const posStart = translateValue2Offset(barValues.start);
    const posEnd = translateValue2Offset(barValues.end);
    setPosition(posStart);
    setWidth(posEnd - posStart);
  }, [barValues]);

  return (
    <div
      className={classNames("slider-bar", { draggable: hasDraggableBars })}
      ref={barRef}
      style={{
        width: `${width}px`,
        transform: `translate(${position}px, 0)`,
      }}
    />
  );
});
