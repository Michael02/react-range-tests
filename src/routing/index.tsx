import React, { Suspense } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Spinner from "components/Spinner";

const MainPage = React.lazy(() => import("pages/MainPage"));

const MainNavigation = () => {
  return (
    <Router>
      <Suspense fallback={<Spinner animation='border' variant='primary' />}>
        <Switch>
          <Route exact path='/' component={MainPage} />
        </Switch>
      </Suspense>
    </Router>
  );
};

export default MainNavigation;
