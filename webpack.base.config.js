const fs = require("fs");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const Dotenv = require("dotenv-webpack");
const title = require("./package").title;

function createSrcDirsMap() {
  const srcPath = path.resolve(__dirname, "src/");
  const srcItems = fs.readdirSync(srcPath);
  const srcDirsMap = {};

  srcItems.forEach((item) => {
    const itemPath = path.resolve(srcPath, item);
    const itemStats = fs.statSync(itemPath);
    const itemIsDir = itemStats.isDirectory();
    if (itemIsDir) {
      srcDirsMap[item] = itemPath;
    }
  });

  return srcDirsMap;
}

module.exports = {
  entry: ["./src/index.tsx"],
  output: {
    path: path.resolve(__dirname, "dist/"),
    filename: "bundle.[chunkhash].js",
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js"],
    alias: createSrcDirsMap(),
  },
  module: {
    rules: [
      {
        test: /\.(t|j)sx?$/,
        loader: "babel-loader",
        exclude: /node_modules/,
      },
      {
        test: /\.s?css$/,
        use: ["style-loader", "css-loader", "sass-loader"],
        exclude: /node_modules/,
      },
      {
        test: /\.(gif|png|jpe?g|svg)$/,
        use: [
          "file-loader",
          {
            loader: "image-webpack-loader",
            options: {
              bypassOnDebug: true,
            },
          },
        ],
        exclude: /node_modules/,
      },
    ],
  },
  devServer: {
    contentBase: path.resolve(__dirname, "public/"),
    historyApiFallback: true,
    host: "0.0.0.0",
    port: 8080,
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, "src/index.ejs"),
      hash: true,
      templateParameters: (compilation, assets, assetTags, options) => {
        return {
          compilation,
          webpackConfig: compilation.options,
          htmlWebpackPlugin: {
            tags: assetTags,
            files: assets,
            options,
          },
          title: title || null,
        };
      },
    }),
    new Dotenv(),
  ],
};
